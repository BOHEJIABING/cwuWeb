import {createApp} from 'vue';
import Antd from 'ant-design-vue';
import App from './App';
import 'ant-design-vue/dist/antd.css';
import * as VueRouter from 'vue-router'
import {axios} from 'axios';
import {message} from 'ant-design-vue';
import cFunc from './common/cFunc.js';
import cValue from './common/cValue';

const app = createApp(App);
app.use(Antd)

// 1. 定义路由组件
// import Home from './views/MainHome'
import BaseImgList from './views/BaseImgList'
import SysConfigList from './views/SysConfigList'
import TimetableList from './views/TimetableList'
import TimetableAdd from './views/TimetableAdd'
import Login from './views/UserLogin.vue'
import UserList from './views/UserList'
import UserRegister from './views/UserRegister'

// 2. 定义一些路由
const routes = [
    {path: '/', component: BaseImgList},
    {path: '/baseImgList', component: BaseImgList},
    {path: '/sysConfigList', component: SysConfigList},
    {path: '/timetableList', component: TimetableList},
    {path: '/timetableAdd', component: TimetableAdd},
    {path: '/login', component: Login},
    {path: '/userList', component: UserList},
    {path: '/register', component: UserRegister},
]

// 3. 创建路由实例并传递 `routes` 配置
const router = VueRouter.createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: VueRouter.createWebHashHistory(),
    routes, // `routes: routes` 的缩写
})

//整个应用支持路由。
app.use(router)
app.config.globalProperties.$axios = axios
app.config.globalProperties.$message = message
app.config.globalProperties.cFunc = cFunc
app.config.globalProperties.cValue = cValue
app.mount('#app')

export default router
