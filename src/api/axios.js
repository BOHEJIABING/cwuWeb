import axios from 'axios'
import qs from 'qs'
// 实例对象
let instance = axios.create({
timeout: 3000,
headers: {
'Content-Type': 'application/x-www-form-urlencoded'
}
})
// 请求拦截器
instance.interceptors.request.use(
config => {
config.data = qs.stringify(config.data) // 转为formdata数据格式
return config
},
error => Promise.error(error)
)
export default instance