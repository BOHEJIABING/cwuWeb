import {reactive} from "vue";
import cFunc from './cFunc'

export const store = {
    state: reactive({
        userName: cFunc.getSessionUserName()
    }),
    setUserName(userName) {
        console.log("setUserName: " + userName)
        this.state.userName = userName
    },
    clearUserName() {
        this.state.userName = "登录"
    }

}
