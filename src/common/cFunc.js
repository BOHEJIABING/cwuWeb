import {message} from 'ant-design-vue';
import axios from "axios"; //导入axios
import router from '../main.js'
import qs from "qs"; //导入qs
import {store} from '../common/cStore'

//设置默认请求头
axios.defaults.headers = {
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
};
export default {

    /***************************************** 各类检查 ***************************************************/
    // 检查值是否有效，全部有效返回true
    checkValueIsAllValid() {
        this.length = arguments.length
        for (let i = 0; i < arguments.length; i++) {
            if (arguments[i] === '' || arguments[i] == undefined || (arguments[i] !== 0 && !arguments[i])) {
                return false
            }
        }
        return true
    },
    // 检查值是否有效，只要有一个有效，返回true
    checkValueIsValid() {
        this.length = arguments.length
        for (let i = 0; i < arguments.length; i++) {
            if (arguments[i] !== '' && arguments[i] != undefined || arguments[i]) {
                return true
            }
        }
        return false
    },

    /**
     * 检查是否参数是否被修改
     * @param value 修改后的值
     * @param originValue 修改前的值
     * @returns {boolean} 是否被修改
     */
    checkValueIsChange(value, originValue) {
        if (value == undefined) {
            return false;
        }
        if (value === originValue) {
            message.error("该项已是选中的状态")
            return false;
        }
        return true
    },

    // 检查用户权限-本地
    checkPermission(permission) {
        let myPermissions = this.getSessionPermissions()
        let userName = this.getSessionUserName()
        if (!this.checkValueIsAllValid(myPermissions, userName)) {
            this.loginInvalid()
            return false
        }
        myPermissions = myPermissions - 0
        if (myPermissions >= 3) {
            message.error("您没有编辑权限！")
            return false;
        }
        if (permission != null && myPermissions >= 2 ) {
            message.error("只有一级及以上的管理员拥有修改管理员权限资格！")
            return false;
        }
        return true
    },

    // 检查是否为图片
    checkIsImg(url) {
        if (!this.checkValueIsAllValid(url)) {
            return false
        }
        if (url.endsWith('.png') || url.endsWith('.jpg') || url.endsWith('.jpeg') ||
            url.endsWith('.webp') || url.endsWith('.gif')) {
            return true
        }
    },

    // 检查是否为视频
    checkIsVideo(url) {
        if (!this.checkValueIsAllValid(url)) {
            return false
        }
        if (url.endsWith('.mp4') || url.endsWith('.webm')) {
            return true
        }
    },
    /*************************************** 登录失效 *****************************************************/
    // 登录失效
    loginInvalid() {
        console.log("login invaild")
        message.error("登录已失效，请重新登录！")
        this.removeSessionUserName()
        store.clearUserName()
        router.push('/login')
    },

    /************************************** 网络访问包装 *******************************************/
    // 网络访问
    cwuRequest(url, params, success, failure) {
        const that = this
        axios.post(url, params, {
            headers: {
                token: sessionStorage.getItem("token"),
            },
        }).then(
            function (res) {
                if (res.data.code === "200" || res.data.code === "1") {
                    success(res.data.result)
                } else {
                    if (res.data.msg === "请先登录") {
                        that.loginInvalid()
                    } else {
                        try {
                            failure(res)
                        } catch (e) {
                            message.error(res.data.msg)
                        }
                    }
                }
            },
            function (res) {
                console.log(res);
                message.error(res);
            }
        )
    },

    // 网络访问-json
    cwuRequestJson(url, params, success, failure) {
        axios.defaults.headers = {
            "Content-Type": "application/json",
        };
        this.cwuRequest(url, params, success, failure)
    },

    // 获取级联选择器数据
    getCascaderOptions(success) {
        this.cwuRequest("/api/condition/getConditionList", '', result => {
           success(result)
        })
    },

    // 检查是否有课表
    checkIsHaveTimetable(condition, success, failure) {
        const timetable = {};
        timetable["firstCondition"] = condition[1];
        timetable["secondCondition"] = condition[2];
        this.cwuRequest("/api/condition/checkIsHaveTimetable", qs.stringify(timetable),
            result => success(result),
            res => failure(res))
    },

    // 上传图片
    uploadImg(e, success) {
        if (!this.checkPermission()) {
            return;
        }
        let formdata = new FormData();
        let file = e.target.files[0];
        if (file == null) {
            message.error("当前未选择图片！");
            return;
        }
        message.loading('上传中……', 0)
        console.log("uploadImg-------")
        formdata.append("file", file);
        try {
            this.cwuRequest("/api/upload", formdata, result => {
                message.destroy()
                message.success("图片上传成功！");
                success(result)
            })
        } catch (e) {
            console.log(e);
        }
    },
    /********************************************** session 操作 **************************************************************/

    // 设置session userName
    setSessionUserName(value) {
        this.setSession("userName", value)
    },
    // 设置session permission
    setSessionPermissions(value) {
        this.setSession("permissions", value)
    },
    // 设置session token
    setSessionToken(value) {
        this.setSession("token", value)
    },
    // session item 赋值
    setSession(itemName, itemValue) {
        window.sessionStorage.setItem(itemName, itemValue)
    },

    // 取session userName
    getSessionUserName() {
        return this.getSession("userName")
    },
    // 取session permission
    getSessionPermissions() {
        return this.getSession("permissions")
    },
    // 取session token
    getSessionToken() {
        return this.getSession("token")
    },
    // session item 取值
    getSession(itemName) {
        return window.sessionStorage.getItem(itemName)
    },

    // session remove
    removeSessionUserName() {
        this.removeSession("userName")
    },

    removeSession(itemName) {
        window.sessionStorage.removeItem(itemName)
    }
}
