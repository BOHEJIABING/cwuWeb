import {ref} from 'vue';

/*** options **/
// 状态选择器
const statusOptions = ref([{
    value: null,
    label: '全部',
}, {
    value: 1,
    label: '可用',
}, {
    value: 0,
    label: '不可用',
}])

// 状态更改选择器
const statusSelectOptions = ref([{
    value: 1,
    label: '设置可用',
}, {
    value: 0,
    label: '设置不可用',
}])

// 模块选择器
const moduleOptions = ref([{
    value: null,
    label: '全部',
}, {
    value: 0,
    label: '首页推荐',
}, {
    value: 2,
    label: '新闻动态',
}, {
    value: 3,
    label: '教学实践',
}, {
    value: 50,
    label: '学生活动',
}, {
    value: 51,
    label: '教师活动'
}]);

// 模块添加选择器
const moduleAddOptions = ref([{
    value: 0,
    label: '首页推荐',
}, {
    value: 2,
    label: '新闻动态',
}, {
    value: 3,
    label: '教学实践',
}, {
    value: 50,
    label: '学生活动',
}, {
    value: 51,
    label: '教师活动'
}]);

// 权限选择器
const permissionsOptions = ref([{
    value: 'null',
    label: '全部',
}, {
    value: '0',
    label: '超级管理员',
}, {
    value: '1',
    label: '一级管理员',
}, {
    value: '2',
    label: '二级管理员',
}, {
    value: '3',
    label: '三级管理员',
}
]);
// 权限更改选择器
const permissionChangeOptions = ref([{
    value: 1,
    label: '设为一级管理员',
}, {
    value: 2,
    label: '设为二级管理员',
}, {
    value: 3,
    label: '设为三级管理员',
}]);

// 课表模块选择器
const timetableModuleOptions = ref([{
    value: 40,
    label: '学生',
}, {
    value: 41,
    label: '教师',
}, {
    value: 42,
    label: '教室'
}])

/*** columns **/
// 系统设置columns
const sysColumns = [{
    name: 'id',
    dataIndex: 'id',
    key: 'id',
}, {
    title: '设置名称',
    dataIndex: 'name',
    key: 'name',
}, {
    title: '主要设置内容',
    dataIndex: 'configMain',
    key: 'configMain',
}, {
    title: '备用设置内容',
    key: 'configSpare',
    dataIndex: 'configSpare',
}, {
    title: '模块',
    key: 'module',
    dataIndex: 'module',
}, {
    title: '状态',
    key: 'status',
    dataIndex: 'status',
}, {
    title: '操作',
    key: 'action',
}];

// 图片列表columns
const baseImgColumns = [{
    name: 'id',
    dataIndex: 'id',
    key: 'id',
    width: 100,
    sorter: {
        compare: (a, b) => a.id - b.id,
        multiple: 3,
    },
}, {
    title: '基础链接',
    dataIndex: 'baseUrl',
    key: 'baseUrl',
    width: 300,
}, {
    title: '详情链接',
    dataIndex: 'detailUrl',
    key: 'detailUrl',
    width: 300,
}, {
    title: '所属模块',
    key: 'module',
    dataIndex: 'module',
    width: 150,
    sorter: {
        compare: (a, b) => a.module - b.module,
        multiple: 3,
    },
}, {
    title: '状态',
    key: 'status',
    dataIndex: 'status',
    sorter: {
        compare: (a, b) => a.status - b.status,
        multiple: 3,
    },
}, {
    title: '操作',
    key: 'action',
}];

const userColumns = [{
    name: 'id',
    dataIndex: 'id',
    key: 'id',
    sorter: {
        compare: (a, b) => a.id - b.id,
        multiple: 3,
    },
}, {
    title: '姓名',
    dataIndex: 'userName',
    key: 'userName',
}, {
    title: '学号/工号',
    dataIndex: 'schoolId',
    key: 'schoolId',
    sorter: {
        compare: (a, b) => a.schoolId - b.schoolId,
        multiple: 3,
    },
}, {
    title: '权限',
    key: 'permissions',
    dataIndex: 'permissions',
    sorter: {
        compare: (a, b) => a.permissions - b.permissions,
        multiple: 3,
    },
}, {
    title: '状态',
    key: 'status',
    dataIndex: 'status',
    sorter: {
        compare: (a, b) => a.status - b.status,
        multiple: 3,
    },
}, {
    title: '操作',
    key: 'action',
}];

// 周列表
const dayOfWeekColumns = [{
    value: 1,
    label: '周一'
}, {
    value: 2,
    label: '周二'
}, {
    value: 3,
    label: '周三'
}, {
    value: 4,
    label: '周四'
}, {
    value: 5,
    label: '周五'
}]

// 课表列
const timetableColumns = [{
    name: 'id',
    dataIndex: 'id',
    key: 'id',
    width: 65,
}, {
    title: '课程表图片',
    dataIndex: 'timetableUrl',
    key: 'timetableUrl',
    width: 300,
}, {
    title: '课程表选中图片',
    dataIndex: 'timetableUrlSelected',
    key: 'timetableUrlSelected',
    width: 300,
}, {
    title: '周几',
    key: 'dayOfWeek',
    dataIndex: 'dayOfWeek',
}, {
    title: '层级',
    key: 'condition',
    dataIndex: 'condition',
    width: 200
}, {
    title: '状态',
    key: 'status',
    dataIndex: 'status',
}, {
    title: '操作',
    key: 'action',
    width: 400
}];
export default {
    statusOptions,
    statusSelectOptions,
    sysColumns,
    moduleOptions,
    moduleAddOptions,
    baseImgColumns,
    userColumns,
    permissionsOptions,
    permissionChangeOptions,
    timetableColumns,
    timetableModuleOptions,
    dayOfWeekColumns
}
