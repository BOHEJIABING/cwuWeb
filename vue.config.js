// const { defineConfig } = require('@vue/cli-service')

// module.exports = defineConfig({
//     transpileDependencies: true,
//     lintOnSave:false
// })
module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: 'http://192.168.31.150:8081/',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '/api': ''
                }
            }
        }
    }
}
